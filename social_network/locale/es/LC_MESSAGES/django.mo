��    _                   �   	  y   �  $   	      2	  ,   S	     �	     �	  5   �	     �	     �	     �	     �	     �	     �	     
     
     
  &   $
     K
     d
     k
     �
     �
     �
     �
     �
      �
     �
     �
     �
     �
      	     *  ,   /     \     d          �     �     �     �     �     �     �     �  
          (   $     M     l     z  -   �  &   �     �  !   �          #     6     Q     g       6   �  -   �     �     �                          ,     :     B     J     Q     ]     l     |     �     �     �  
   �     �     �     �     �     �     �       	        #     >     C     P     ^  k  b  �   �  y   X  1   �  $     =   )     g     u  6   �     �     �     �     �     �     �     �     �     	  .      (   O     x          �     �     �     �     �  $   �               %     .  +   H     t  C   {     �     �     �     �               ,     A     M     d     k     �     �     �     �     �     �  /      ,   0  !   ]  ,        �     �     �     �     �       F   !  ,   h  	   �     �  
   �     �  
   �     �     �     �     �  	   	                5  	   L     V     t     �     �     �     �     �     �     �           1     :  #   F     j  	   p     z     �     A           "   F      _                (       *          +   G   -   ?       ]   4          \   H       U   Y   O   .      &       ;             P          N   X   =       I   3      [                             J      W   5      /       E   7          ,       B   6       D   C             K   ^   0           T   
       !   M   8   	   )   2         L   <   :   V      S              Z           R      @                >   9   %   '       1   #           Q         $    
                                        Members (displaying %(members_count)s of %(members_count)s)
                                     
                                        Members (displaying 6 of %(members_count)s)
                                     A new post has been added to a group A validation error has occurred. Add group administrators (other than you)... Administrator Administrators An error has occurred while processing your request'. Cancel Change Clean Comment Create Create Group Creator Description Enter a link Enter group description and details... Enter group name here... Follow Friend request sent to  Friends Friendship Requests Group Image Group Picture Groups I want this to be a closed group Join Member Members Membership Requests Membership request sent to group Name No %(verbose_name)s found matching the query Picture Place your content here... Profile Picture Request Friendship Request Membership Request Sent See more details... Select Select Social Groups Send Send Friend Request Share Link Share a photo Share something with friends and groups. Share something with the group Social Groups Stop Following The group post has been successfully removed. The item has been successfully shared. There are no group to show... There are no membership requests. Wall Write your comment Write your comment here... Write your comment... Write your link here... You and  You must type a comment to post in your social groups. Your request has been successfully processed. accepted administrators are friends closed comment cover image creation date creator decider denied description friend request friend requests group group feed item group membership request group membership requests group post group posts image message name profile comment profile comments receiver requester sent you a friend request. site social group social groups url Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-03-10 11:24-0600
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 
                                        Miembros (mostrando %(members_count)s de %(members_count)s)
                                     
                                        Miembros (mostrando 6 de %(members_count)s)
                                     Se ha realizado una publicación en una comunidad Ha ocurrido un error de validación. Agregue administradores de la comunidad (además de usted)... Administrador Administradores Ha ocurrido un error intentando procesar su solicitud. Cancelar Cambiar Limpiar Comentar Crear Crear Comunidad Creador Descripción Introduzca un vínculo Entre la descripción de la comunidad aquí... Entre el nombre de la comunidad aquí... Seguir Solicitud de amistad enviada a  Amigos Solicitudes de Amistad Imagen de Comunidad Imagen de Comunidad Comunidades Quiero que sea una comunidad cerrada Unirse Miembro Miembros Solicitudes de Membresía Solicitud de membresía enviada a comunidad Nombre No se han encontrado %(verbose_name)s que coincidan con la consulta Imagen Escriba su contenido aquí... Foto de Perfil Solicitar Amistad Solicitar Membresía Solicitud Enviada Ver más detalles... Seleccionar Selecciona Comunidades Enviar Enviar Solicitud de Amistad Compartir Enlace Compartir una foto Comparte con amigos y grupos. Comparte algo con el grupo Comunidades Dejar de Seguir La publicación ha sido eliminada exitosamente. El elemento ha sido compartido exitosamente. No hay comunidades que mostrar... No hay solicitudes de membresía pendientes. Muro Escriba su comentario Escriba su comentario aquí... Escriba su comentario... Entre su enlace aquí... Usted y  Usted debe escribir un comentario para poder postear en una comunidad. Su solicitud ha sido procesada exitosamente. acceptada administradores son amigos cerrada comentario portada fecha de creación creador responsable de decisión rechazada descripción solicitud de amistad solicitudes de amistad comunidad elemento de feed de comunidad solicitud de membresía solicitudes de membresía publicación en comunidad publicaciones en comunidad imagen mensaje nomre comentario en perfil de usuario comentarios en perfil de usuario receptor solicitante le envió una solicitud de amistad. sitio comunidad comunidades url 